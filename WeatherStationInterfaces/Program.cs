﻿using WeatherStationInterfaces.Interfaces;
using WeatherStationInterfaces.Implementations;

namespace WeatherStationInterfaces
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            WeatherStation weatherStation = new WeatherStation();

            IObserver observer1 = new StatisticReport();
            IObserver observer2 = new CurrentConditionsReports();

            weatherStation.Register(observer1);
            weatherStation.Register(observer2);
            
            weatherStation.Start(10);
        }
    }
}