﻿namespace WeatherStationInterfaces.Interfaces
{
    /// <summary>
    /// Provides methods to work with subscribers.
    /// </summary>
    public interface IObservable
    {
        /// <summary>
        /// Registers new subscriber.
        /// </summary>
        /// <param name="observer">New subscriber.</param>
        void Register(IObserver observer);
        
        /// <summary>
        /// Unregisters subscriber.
        /// </summary>
        /// <param name="observer">Subscriber to unregister.</param>
        void Unregister(IObserver observer);
        
        /// <summary>
        /// Notifies all subscribers about an event.
        /// </summary>
        void Notify();
    }
}