﻿namespace WeatherStationInterfaces.Interfaces
{
    /// <summary>
    /// Provides method to subscribe.
    /// </summary>
    public interface IObserver
    {
        /// <summary>
        /// Receives new weather data.
        /// </summary>
        /// <param name="sender">Weather data provider.</param>
        /// <param name="e">Weather data.</param>
        void Update(object sender, WeatherEventArgs e);
    }
}