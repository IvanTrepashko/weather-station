﻿using System;
using WeatherStationInterfaces.Interfaces;

namespace WeatherStationInterfaces.Implementations
{
    /// <summary>
    /// Represents class for collecting reports about current weather.
    /// </summary>
    public class CurrentConditionsReports : IObserver
    {
        /// <inheritdoc cref="IObserver"/>
        public void Update(object sender, WeatherEventArgs e)
        {
            Console.WriteLine("--------------------------");
            Console.WriteLine($"Current temperature: {e.Temperature}");
            Console.WriteLine($"Current pressure: {e.Pressure}");
            Console.WriteLine($"Current humidity: {e.Humidity}");
        }
    }
}