﻿using System;
using System.Threading;
using WeatherStationInterfaces.Interfaces;

namespace WeatherStationInterfaces.Implementations
{
    /// <summary>
    /// Represents weather station.
    /// </summary>
    public class WeatherStation : IObservable
    {
        private readonly WeatherData weatherData;
        private EventHandler<WeatherEventArgs> observers;
        
        /// <summary>
        /// Creates new instance of <see cref="WeatherStation"/> class.
        /// </summary>
        public WeatherStation()
        {
            this.weatherData = new WeatherData();
        }
        
        /// <inheritdoc cref="IObservable"/>
        public void Register(IObserver observer)
        {
            this.observers += observer.Update;
        }

        /// <inheritdoc cref="IObservable"/>
        public void Unregister(IObserver observer)
        {
            this.observers -= observer.Update;
        }

        /// <summary>
        /// Starts collecting data about weather.
        /// </summary>
        /// <param name="collectsCount">Number of collects.</param>
        public void Start(int collectsCount)
        {
            while (collectsCount != 0)
            {
                ((IObservable)this).Notify();
                collectsCount--;
                Thread.Sleep(1000);
            }
        }

        /// <inheritdoc cref="IObservable"/>
        void IObservable.Notify()
        {
            this.observers?.Invoke(this, this.weatherData.UpdateData());
        }

        
        /// <summary>
        /// Represents class for collecting data.
        /// </summary>
        private class WeatherData
        {
            private readonly Random rnd;

            /// <summary>
            /// Creates new instance of <see cref="WeatherData"/>
            /// </summary>
            public WeatherData()
            {
                this.rnd = new Random();
            }
        
            /// <summary>
            /// Returns new <see cref="WeatherEventArgs"/> data.
            /// </summary>
            /// <returns>New <see cref="WeatherEventArgs"/> data.</returns>
            public WeatherEventArgs UpdateData()
            {
                int pressure = rnd.Next(1, 1000);
                int humidity = rnd.Next(1, 100);
                double temperature = rnd.Next(-30, 30);
            
                return new WeatherEventArgs(humidity, pressure, temperature);
            }
        }
    }
}