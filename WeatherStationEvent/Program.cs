﻿using WeatherStation.Implementations;

namespace WeatherStation
{
    public static class Program
    {
        public static void Main()
        {
            var currentConditionsReport = new CurrentConditionsReport();
            var statisticReport = new StatisticReport();

            WeatherStation station = new WeatherStation();

            station.DataUpdate += currentConditionsReport.CollectData;
            station.DataUpdate += statisticReport.CollectData;
            
            station.Start(10);
        }
    }
}
