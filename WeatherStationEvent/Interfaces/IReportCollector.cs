﻿namespace WeatherStation.Interfaces
{
    /// <summary>
    /// Provides method for collecting data about weather.
    /// </summary>
    public interface IReportCollector
    {
        /// <summary>
        /// Collects data about weather.
        /// </summary>
        /// <param name="sender">Weather data provider.</param>
        /// <param name="e">Weather data.</param>
        public void CollectData(object sender, WeatherEventArgs e);
    }
}