﻿using System;
using System.Threading;

namespace WeatherStation
{
    /// <summary>
    /// Represents weather station.
    /// </summary>
    public class WeatherStation
    {
        private readonly WeatherData weatherData;
        
        /// <summary>
        /// Represents event, when new weather data is received.
        /// </summary>
        public event EventHandler<WeatherEventArgs> DataUpdate; 

        /// <summary>
        /// Creates new instance of <see cref="WeatherStation"/> class.
        /// </summary>
        public WeatherStation()
        {
            this.weatherData = new WeatherData();
        }

        /// <summary>
        /// Starts collecting data about the weather.
        /// </summary>
        /// <param name="collectsCount">Number of collects.</param>
        public void Start(int collectsCount)
        {
            while (collectsCount != 0)
            {
                this.OnDataUpdate();
                collectsCount--;
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// Supplies all subscribers new weather data.
        /// </summary>
        protected virtual void OnDataUpdate()
        {
            this.DataUpdate?.Invoke(this, this.weatherData.UpdateData());
        }

        /// <summary>
        /// Represents class for collecting data.
        /// </summary>
        private class WeatherData
        {
            private readonly Random rnd;

            /// <summary>
            /// Creates new instance of <see cref="WeatherData"/>
            /// </summary>
            public WeatherData()
            {
                this.rnd = new Random();
            }
        
            /// <summary>
            /// Returns new <see cref="WeatherEventArgs"/> data.
            /// </summary>
            /// <returns>New <see cref="WeatherEventArgs"/> data.</returns>
            public WeatherEventArgs UpdateData()
            {
                int pressure = rnd.Next(1, 1000);
                int humidity = rnd.Next(1, 100);
                double temperature = rnd.Next(-30, 30);
            
                return new WeatherEventArgs(humidity, pressure, temperature);
            }
        }
    }
}