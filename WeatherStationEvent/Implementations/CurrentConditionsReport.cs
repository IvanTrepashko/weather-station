﻿using System;
using WeatherStation.Interfaces;

namespace WeatherStation.Implementations
{
    /// <summary>
    /// Represents class for collecting reports about current weather.
    /// </summary>
    public class CurrentConditionsReport : IReportCollector
    {
        /// <inheritdoc cref="IReportCollector"/>
        public void CollectData(object sender, WeatherEventArgs e)
        {
            Console.WriteLine("--------------------------");
            Console.WriteLine($"Current temperature: {e.Temperature}");
            Console.WriteLine($"Current pressure: {e.Pressure}");
            Console.WriteLine($"Current humidity: {e.Humidity}");
        }
    }
}