﻿using System;
using WeatherStation.Interfaces;

namespace WeatherStation.Implementations
{
    /// <summary>
    /// Represents class for creating statistics about weather.
    /// </summary>
    public class StatisticReport : IReportCollector
    {
        private double pressure;
        private double temperature;
        private double humidity;

        private int collectionsCount;
        
        /// <inheritdoc cref="IReportCollector"/>
        public void CollectData(object sender, WeatherEventArgs e)
        {
            this.pressure += e.Pressure;
            this.temperature += e.Temperature;
            this.humidity += e.Humidity;
            this.collectionsCount++;

            Console.WriteLine("--------------------------");
            Console.WriteLine($"Average temperature: {this.temperature/this.collectionsCount:F1}");
            Console.WriteLine($"Average humidity: {this.humidity/this.collectionsCount:F1}");
            Console.WriteLine($"Average pressure: {this.pressure/this.collectionsCount:F1}");
        }
    }
}