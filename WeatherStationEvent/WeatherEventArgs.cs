﻿using System;

namespace WeatherStation
{
    /// <summary>
    /// Represents event arguments about current weather.
    /// </summary>
    public class WeatherEventArgs : EventArgs
    {
        private int humidity;
        private int pressure;

        /// <summary>
        /// Creates instance of <see cref="WeatherEventArgs"/> class.
        /// </summary>
        /// <param name="humidity">Humidity.</param>
        /// <param name="pressure">Pressure.</param>
        /// <param name="temperature">Temperature.</param>
        public WeatherEventArgs(int humidity, int pressure, double temperature)
        {
            this.Humidity = humidity;
            this.Pressure = pressure;
            this.Temperature = temperature;
        }

        /// <summary>
        /// Gets temperature.
        /// </summary>
        public double Temperature { get; }

        /// <summary>
        /// Gets humidity.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when humidity is less than 0 or more than 100.</exception>
        public int Humidity
        {
            get => this.humidity;
            private init
            {
                if (value < 0 || value > 100)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                this.humidity = value;
            }
        }

        /// <summary>
        /// Gets pressure.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when pressure is less or equal to 0.</exception>
        public int Pressure
        {
            get => this.pressure;
            private init
            {
                if (value <= 0)
                {
                    throw new ArgumentOutOfRangeException(nameof(value));
                }

                this.pressure = value;
            }
        }
    }
}